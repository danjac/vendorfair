SAAS for renting and allocating stalls at markets and other events

[[docs/images/kayle-kaupanger-J8ksCswaBYo-unsplash.jpg]]

Photo by <a href="https://unsplash.com/@notaphotographer?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Kayle Kaupanger</a> on <a href="https://unsplash.com/photos/man-wearing-grey-bubble-jacket-J8ksCswaBYo?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>
 
 
[Business Pitch](docs/pitch.md)

[Data Model](docs/data_model.md)

[Pages](docs/pages.md)
