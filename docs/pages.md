# Pages

## Landing Page

The hero/landing page for the site with marketing info.

**Access:** Everyone

## About Page

Specific details about the site.

**Access:** Everyone

## Signup

Form to register as new user. There should be two workflows: Vendor or Organizer. 

Signup consists of multiple steps, the first step is generic info (name, email, password etc) followed by specific steps for Vendor or Organizer.

When signing up as Vendor, once initial details (name, email etc) are added, the user should add Vendor name, description and other basic details, and is redirected to the Vendor Dashboard. 

For an Organizer, the user enters relevant details and is redirected to a success page, and will be able to access the Organizer Dashboard once registration is approved by the admins.

Should support social auth signup (Google etc) as appropriate.

**Access:** Everyone

## Login

Generic login form for authenticating all users. Depending on user type, should redirect to Vendor or Organizer Dashboard.

Also includes links to handle password retrieval etc. as standard.

Should support social auth login (Google etc) as appropriate.

**Access:** Everyone

## Upcoming Events

Shows all current and future events handled by the site. Each event is shown in a card. Clicking on link on the card goes to the **Event Detail** page.

**Access:** Everyone

## Event Detail

Shows details of an event:

* Name and description
* Vendor details
* Map location
* Dates
* Image
* List of confirmed vendors
* "Reserve" button (if user is a Vendor: if no longer reservable, show note instead)
* "Reservation Details" link (if user is a Vendor who has made reservation to this event) 
* "Edit" button (if user is Organizer of this event)
* "Publish" button (if user is Organizer of this event)
* Pricing details and reservation conditions (if user is Vendor)

If user is Organizer this detail may be shown if the event has not been published yet.

**Access:** Everyone

## Vendor Dashboard

Dashboard for individual vendor. Includes the following pages:

* Reservations
* Upcoming Events
* Organizers
* Private Messages
* Documents
* Settings

**Access:** Vendors

## Organizer Dashboard

Dashboard for individual organizer. Includes the following pages:

* Events
* Venues
* Vendors
* Private Messages
* Settings

**Access:** Organizers

## Vendor Settings

Settings for this vendor, e.g. vendor profile.

**Access:** Vendors

## Organizer Settings

Settings for this organizer, e.g. organizer profile.

**Access:** Organizers

## Documents

Documents list for vendor. Vendors typically need to keep PDFs or other files when applying for a reservation, for example a food hygiene certificate or product brochure. These can be uploaded during the reservation process, but are all stored here. Functionality should include removal of documents and uploading new documents. Documents should be searchable.

**Access:** Vendors

## Stalls

List of stalls at Event. Each item should show status (open, reserved) with name of vendor. It should be possible to add/remove stalls.

**Access:** Organizers

## Create Event

Shows event form to organizer. When creating an event, the Organizer specifies the initial number of stalls, which are added when the Event is created.

Organizer can publish this event immediately.

Redirects to **Event Details** page.

**Access:** Organizers

## Create Venue

Form to add a venue (it should also be possible to add venue when creating event, through a modal).

Redirects to **Venue Details** page.

**Access:** Organizers

## Venues

Shows searchable list of venues. Clicking on venue shows **Venue Details** page.

**Access:** Organizers

## Organizer Reservations 

List of reservations for all events for an Organizer. Should be filterable by state and searchable (vendor name, event name etc).

Clicking on item in list should redirect to **Reservation Details** page.

**Access:** Organizers

## Organizer Reservation Details

Details shown to Organizer for Reservation, including the vendor details, payment status etc.

Should have actions for "Cancel", "Approve", "Reject" etc depending on status.

**Access:** Organizers

## Organizer Details

Contains public details of an Organizer.

If Vendor, there should be a button to request Associate status (redirect to Associate Request form).

**Access:** Everyone

## Associate Request

Form for a vendor to request associate status to an Organizer, if the Organizer has associate memberships available. Associate membership usually incurs benefits such as lower prices and/or priority for certain events.

In some cases payment may be required, on a monthly/annual basis. 

**Access:** Vendors

## Organizer Venue Details

Shows details of venue, including map location, contact details etc.

Has "Create Event" button which will automatically select this venue on selection.

Has "Edit" button that redirects to **Edit Venue** page.

**Access:** Organizers

## Venue Events

List of events for a specific venue. 

**Access:** Organizers

## Reserve Event

Shows reservation form to vendor. Should include in addition to form:

* Terms and conditions of reservation + checkbox
* Pricing details
* Checklist of requirements (e.g. documents, suitable products etc)

Form should redirect to payment workflow on submit if payment required, otherwise returns to **Vendor Reservations** page. The reservation will usually wait for confirmation by the Organizer.

**Access:** Vendors

## Vendor Reservations

Shows (filterable) list of reservations for this vendor. Part of Vendor Dashboard.

User can filter upcoming, completed, cancelled events etc. and search based on venue/event name.

List should show dates/times, stall number and link to **Reservation Detail** page.

Tab/switch button should render calendar view instead of list. Calendar view shows just time and link to Reservation Detail page.

**Access:** Vendors

## Organizer Reservations

Shows (filterable) list of reservations for events for this organizer. Part of Organizer Dashboard.

By default should prioritize requested reservations. Each item should include the event, vendor and additional info e.g. associate membership.

Clicking on item should redirect to **Reservation Detail** page.

**Access:** Organizers

## Vendor Reservation Detail 

Shows details of reservation including:

* Event details (vendor, map, dates etc)
* Reservation details (stall, payment details etc)
* "Cancel" button (if appropriate)
* "Contact Organizer" form

The Contact Organizer form sends a Private Message to an Organizer, and should appear on their **Private Messages** page.

**Access:** Vendors

## Private Messages 

List of private messages, appear in the Organizer or Vendor dashboard. Should be searchable by name. List should have subject, date, name and "is read". Clicking on a message should redirect to **Private Message Detail** page.

**Access:** Organizers, Vendors

## Private Message Detail

Detail of a private message. Navigating to this page automatically marks it read. User can reply to message in "Reply" form/button.

**Access:** Organizers, Vendors


