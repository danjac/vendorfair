# Pitch

In the UK and other countries in Europe, markets, fairs, exhibitions and other events are hugely popular. A typical maker or vendor at such events can make hundreds or even thousands of pounds or Euro at such events at peak times, for example during the tourist season in their home town or in the run-up to Christmas. While some vendors are full-time, others sell their products as a hobby or to supplement their main income. Although services such as Etsy provide a means to sell products world-wide from their home, real-world markets in the right place and time can be more lucrative, with immediate cash in hand and no overhead in shipping.

Currently control over events and venues is managed by organizers. An organizer may rent a venue, such as an art gallery, school gym hall, or purpose-build indoor or outdoor market location for a specific event, and then allocate stalls or booths to vendors on some ad-hoc basis. Sometimes an organizer is a private individual or company, other times a cooperative of the makers themselves. In any case, there is a lot of overhead in managing these events, allocating stalls in an open and fair manner, keeping vendors up to date with new events, collecting payments and deposits, re-allocating stalls in case of cancellation, and so forth.

From a vendor's point of view the ad-hoc nature of this system means that it is difficult to keep on top of where markets are happening, who to contact to secure a stall, where to send payment and how to get a refund in case of cancellation, and keeping all their documentation up to date with the organizers. 

This solution aims to solve these problems for both organizers and vendors. The service would provide organizers with a dashboard where they can create new events, manage payments, allocate stalls and keep vendors up to date. Vendors would be informed of new events, apply for a stall with minimum fuss and overhead, keep track of all their upcoming events, and stay in contact with organizers at all times.

## Business Model

A number of business models are possible, whether strictly for-profit or run on a social enterprise basis. 

1. Per booking: an amount is charged per reservation, for example if an event has 50 bookings, charge might be a fixed amount (e.g. 5 GBP) per reservation or a percentage of the reservation fee.
2. Subscription: a fixed amount is charged per month, regardless of number of events and reservations, e.g. 30 GBP.

In either case, the organizers, not the vendors, pay for the service. Vendors pay individual organizers for reservations and/or associate membership.

In the case of option 2., subscriptions, there may be a graded membership e.g.: free for up to 3 events a year; 10 GBP/month for up to 12 events; 30 GBP for unlimited events.

The exact business model and amounts to be charged should be weighed based on careful market analysis. Event organizers in the UK alone can vary in size from village committees to large corporate event managers, and their budgets will likewise vary. The larger organizers however will also likely have their own bespoke software for events management and are unlikely to be initial customers.

Another potential larger customer may be local councils, however again they may either have their own software or have strenuous purchasing requirements.

In terms of international reach, it is most likely that due to different laws, regulations and other factors, it would be easier to host different instances of the service in different countries/domains. This is not an immediate concern, but would be sensible to build the site with allowances for internationalization and localization from the start.

## Links

### Existing sites/products

* Event Owl: https://eventowl.co.uk/
* StallFinder: https://www.stallfinder.com/
* Marketsquaregroup: https://www.marketsquaregroup.co.uk/
