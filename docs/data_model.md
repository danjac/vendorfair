# Data Model
 
## Organizers

Organizers can be companies, nonprofits, cooperatives, etc. They manage (own or have agreement with) one or many **venues** and organize **events**.  An organizer has a home page with upcoming events, contact details etc. A **user** may belong to one organizer. This user can register events and view events, **reservations**, **payments** and **vendors**.

In some cases an organizer can have associated members, e.g. vendors may belong to a cooperative. Associate vendors may receive certain benefits, such as a reduction in prices or priority in allocating stalls, depending on the event.

An organizer may also be the owner of one or multiple venues, rather than a middleman or agent between venue and vendor.

## Venues

**Events** take place in venues. A venue can be an entire building, park, school gym hall, art gallery etc.  A venue has a home page with contact details, map etc. and upcoming events.

An **organizer** can work with multiple venues. Payments and other arrangements with the venue are outside of scope, but it should be possible for an organizer to store their own notes on the venue e.g. a contact person.

Venue info is shown with each event: it is possible for multiple organizers to work with the same physical venue, but will have their own specific data on that venue.

A venue may include a max capacity of people and stalls, but an event may not use that full capacity (e.g. only some rooms may be available on that date).

## Events

Events are markets and other happenings, where **vendors** sell their products at a **venue**. Events are managed by an **organizer**. Vendors can reserve a **stall** at an event. An event has a home page with details fo the venue, list of vendors, etc. 

Events have a set number of stalls depending on the size of the venue, and have certain special conditions (e.g. food hygiene certificate). Events may also have specific product selection, for example an artists or farmers market.

**Nice to have:** feedback on an event from vendors to organizers. 

## Stalls

A table, stand or booth at a **venue** where a **vendor** sells their product. A vendor must reserve a stall before an event.  A vendor must make a **reservation** on the stall to ensure they have a place. 

A stall has an allocated number, indicating location in a venue.  The actual physical stall may be provided by the vendor, **venue** or **organizer** depending on the situation, but this information should be provided in the **event** detail.

**Nice to have:** floor plan of venue with numbered stalls, indicating your own stall. 

## Reservation

A booking for a **stall** made by a **vendor** for an **event**. Depending on conditions, the reservation may be free or a payment is required to the **organizer** (perhaps with a deposit). The vendor may cancel a reservation depending on conditions. 

Reservations require approval by the organizer. If payment is required,  a reservation may only be approved once a payment is made (this can be waived for individual vendors at the organizer's discretion). Some events may require a deposit to be paid to make a reservation and full payment within a specified period.

A reservation must be made for the full length of the event. For example, if an event is two days, the vendor must book the full two days. The event will typically have a time limit on when a reservation can be made (e.g. must be within 30 days) and for cancellation (e.g. within one week).

Often an event will be heavily in demand by vendors due to a favourable time and/or location. It is up to the organizer to determine how these are allocated, for example associate members may receive priority in getting a place.

## Payments

Individual payment made by a **vendor** for a **reservation**.  In some cases, a deposit is required (e.g. 10% of the full payment). Payments are made through a third-party provider e.g. Stripe.

In certain cases, a payment may be a refund returned to the vendor, e.g. in case of a cancellation on the part of the vendor or the **event**.

## Vendors

Vendors sell their products at a **stall** at an **event**.  They can make a **reservation** for a stall. A vendor has a profile page with links to their site, Facebook page etc.  The vendor **users** should be able to see all their reservations. A vendor may also provide specific documentation, such as a food safety certificate, that can be displayed to **organizers** when applying for a stall.

## Users

A user can belong to **organizer** and/or a **vendor**.  When a user joins the site, they can register as a new organizer or a vendor. 

## Private Messages

Messages sent between an **organizer** and a **vendor**. These will also be emailed to the other party. Allows private communication between the two parties, for example questions on an event, payment issues etc.
